// import page from "./[...slug]";
// export default page;

import { getAll, getBySlug } from '../lib/api'
import { useRouter } from 'next/router'
import ErrorPage from 'next/error'
import Layout from '../components/layout'
import Nav from '../components/nav'
import React, { Component } from 'react';


function Index(props) {

  // const router = useRouter()
  // if (!router.isFallback && !props.page?.slug) {
  //   return <ErrorPage statusCode={404} />
  // }


  return (
    <Layout meta={props.page || {}}>
      <div className="container">
      <div class="row">
        <div class="col-md-8"><h1>{props.page?.title}</h1></div>
        <div class="col-md-4"><Nav pages={props.pages} /></div>
      </div>
      </div>
    </Layout>)
}


export async function getStaticPaths() {

  const pages = await getAll()
  const paths = pages.map(page => page.slug)
  console.log(JSON.stringify(paths))
  
  return { paths, fallback: true }
}


export async function getStaticProps({ params,  preview = false }) {

  const page = await getBySlug(params, preview)
  const pages = await getAll(preview);

  return { props: { page,  pages } }
}
  
export default Index