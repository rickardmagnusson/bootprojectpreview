import Head from 'next/head'

export default function Meta(page) {
    return (
      <Head>
          <meta name="description" content={page?.title}/>
          <link rel="stylesheet" type="text/css" href="/css/bootstrap-grid.css" />
          <link rel="stylesheet" type="text/css" href="/css/template.css" />
      </Head>
    )
  }
  