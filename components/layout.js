import Footer from './footer'
import Meta from './meta'

export default function Layout({page, children}) {
    return (
        <>
            <Meta meta={page} />
                <main>{children}</main>
            <Footer />
        </>
      )
  }