


const Navigation = ({pages}) => {
    return(
        <ul>{pages && pages.map(p=> <li><a href={`${p.slug=='/home' ? '/': p.slug}`}>{p.title}</a></li>)}</ul>
    )
}

export default Navigation;