require('dotenv').config()

module.exports = {
  //  exportTrailingSlash: true,
  experimental: {
    rewrites: async () => [
      { source: '/home', destination: '/' }
    ]
  },
  // useFileSystemPublicRoutes: false,
  // generateBuildId: async () => {
  //       return process.env.NODE_ENV === 'production' ? 'production' : 'dev';
  // },
  env: {
    PROJECT_ID: process.env.PROJECT_ID,
  },
}