import client from '@sanity/client'

const options = {
  // Find your project ID and dataset in `sanity.json` in your studio project
  dataset: 'production',
  projectId: process.env.PROJECT_ID,
  useCdn: process.env.NODE_ENV === 'production',
}

export default client(options)

export const previewClient = client({
  ...options,
  useCdn: false,
  token: process.env.API_TOKEN,
  withCredentials: true
})
