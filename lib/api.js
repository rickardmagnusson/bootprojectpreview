
import client, { previewClient } from './sanity'

const sanity = preview => (preview ? previewClient : client)

const query = `
*[_type == "page"] | order(sortorder){ 
  'slug': slug.current
  ,title
}
`

const slugquery =`
*[_type == "page" && slug.current == $slug] { 
  'slug': slug.current
  ,title
}`

async function getAll(preview) {
    return await sanity(preview).fetch(query)
}

async function getBySlug(params, preview) {
  var slug = params && params.slug && params.slug!=undefined ? `/${params.slug}` : '/home'
  const data = await sanity(preview).fetch(slugquery,{ slug })

  return data[0]
}

export { getAll, getBySlug }